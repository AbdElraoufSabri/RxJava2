package utils;

import io.reactivex.Observable;

public class OBSERVABLE {
  public static Observable<String> just = Observable.just("Alpha", "Beta", "Gamma", "Delta",
          "Epsilon");
}
