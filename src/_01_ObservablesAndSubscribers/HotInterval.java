package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class HotInterval {
    public static void main(String[] args) {
        ConnectableObservable<Long> publish = Observable.interval(1, TimeUnit.SECONDS).publish();

        publish.subscribe(s -> System.out.println("Observer 1:" + s));

        publish.connect();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        publish.subscribe(s -> System.out.println("Observer 2:" + s));

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
