package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;

public class ItWasMyError {
    public static void main(String[] args) {
        Observable.error(new Exception("Crash and burn!"))
                .subscribe(i -> System.out.println("RECEIVED: " + i),
                Throwable::printStackTrace,
                () -> System.out.println("Done!"));
    }
}
