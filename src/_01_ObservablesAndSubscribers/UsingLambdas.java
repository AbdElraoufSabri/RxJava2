package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import utils.OBSERVABLE;


public class UsingLambdas {
  public static void main(String[] args) {
    Consumer<Integer> onNext = integer -> System.out.println("Received: " + integer);
    Action onComplete = () -> System.out.println("Done");
    Consumer<Throwable> onError = Throwable::printStackTrace;

    Observable<String> just = OBSERVABLE.just;

    Disposable subscribe = just
            .map(String::length)
            .filter(integer -> integer >= 5)
            .subscribe(onNext, onError, onComplete);
  }
}