package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;
import utils.OBSERVABLE;

public class IamASingle {
    public static void main(String[] args) {
        Observable<String> just = OBSERVABLE.just;

        just.first("Nil")
                .subscribe(System.out::println);
    }
}
