package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import utils.OBSERVABLE;

public class ImplementObserver {
  public static void main(String[] args) {
    Observable<String> src = OBSERVABLE.just;

    Observer<Integer> observer = new Observer<Integer>() {
      public void onSubscribe(Disposable d) {
        System.out.println("onSubscribe");
      }

      public void onNext(Integer integer) {
        System.out.println("onNext");
        System.out.println("Received: " + integer);

      }

      public void onError(Throwable e) {
        e.printStackTrace();
      }

      public void onComplete() {
        System.out.println("onComplete");
      }
    };

    src.map(String::length).filter(i -> i >= 5)
            .subscribe(observer);

  }
}
