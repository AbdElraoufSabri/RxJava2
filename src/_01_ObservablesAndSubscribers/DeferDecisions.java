package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;

public class DeferDecisions {
    private static int start = 1;
    private static int count = 5;

    public static void main(String[] args) {
        Observable<Integer> source = Observable.range(start, count);
        source.subscribe(i -> System.out.println("Observer 1: " + i));

        //modify count
        count = 10;
        source.subscribe(i -> System.out.println("Observer 2: " + i));

        System.out.println("no change");

        System.out.println("Using defer");
        Observable<Integer> defer = Observable.defer(() -> Observable.range(start, count));
        defer.subscribe(i -> System.out.println("Observer 1: " + i));

        count = 2;
        defer.subscribe(i -> System.out.println("Observer 2: " + i));
    }
}
