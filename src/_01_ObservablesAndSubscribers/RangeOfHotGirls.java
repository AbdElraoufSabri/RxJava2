package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;

public class RangeOfHotGirls {
    public static void main(String[] args) {
        Observable.range(1,10)
                .subscribe(s -> System.out.println("RECEIVED: " + s));
    }
}
