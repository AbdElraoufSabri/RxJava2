package _01_ObservablesAndSubscribers;


import io.reactivex.Observable;

public class create {
  public static void main(String[] args) {
    Observable<String> source = Observable.create(emitter -> {
      emitter.onNext("1");
      emitter.onNext("2");
      emitter.onNext("3");
      emitter.onNext("4");
      emitter.onNext("5");
      emitter.onNext("6");
      emitter.onComplete();
    });

    source.subscribe(s -> System.out.println("Received: " + s));
  }
}