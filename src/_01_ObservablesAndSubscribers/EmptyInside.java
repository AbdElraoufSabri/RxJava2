package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;

public class EmptyInside {

    public static void main(String[] args) {
        Observable<String> empty = Observable.empty();

        empty.subscribe(System.out::println,
                Throwable::printStackTrace,
                () -> System.out.println("Done!"));
    }
}
