package _01_ObservablesAndSubscribers;

import io.reactivex.Observable;

public class CallMeBack {
    public static void main(String[] args) {
        Observable.fromCallable(() -> 1 / 0)
                .subscribe(integer -> System.out.println("Received: " + integer),
                        e -> System.out.println("Error Captured: " + e));
    }
}
